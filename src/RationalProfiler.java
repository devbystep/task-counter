/**
 * Created by Icenord on 14.03.14.
 */

import java.util.ArrayList;
import java.util.List;

public class RationalProfiler {

    List<String> profileStrings;


    // конструктор
    public RationalProfiler() {
        profileStrings = new ArrayList<String>();
    }

    public void addProfilerString(String str) {
        profileStrings.add(str);

    }

    public List<String> getProfilerStringList() {
        return profileStrings;
    }

	/*public String getProfilerString() {
        StringBuilder builder = new StringBuilder();

		for (String str : profileStrings) {
			builder.append(str);
			builder.append("\n");
		}

		return builder.toString();
	} */

}
