import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Icenord on 14.03.14.
 */
public class TaskCounter {

    public static void main(String[] args) throws IOException {
        BufferedReader eon = new BufferedReader(new InputStreamReader(System.in));
        boolean boo;

        do {

            int taskNumber = counter.readNumber("Enter a number of TASK:");
            switch (taskNumber) {
                case 1:
                    if (args.length != 0) {
                        System.out.println("Arguments are avalable! ");
                        counter.printResults(counter.correctNumber(args));

                    } else
                        counter.printResults();
                    break;

                case 2:
                    Rational fract1 = new Rational(1, Rational.getChislitel(),
                            Rational.getZnamenatel(), true);

                    Rational fract2 = new Rational(2, Rational.getChislitel(),
                            Rational.getZnamenatel(), true);

                    System.out.println("");
                    Rational fract000 = fract1.add(fract2, true, false);
                    System.out.println("");
                    Rational fract001 = fract1.mult(fract2, true);
                    System.out.println("");
                    Rational fract002 = fract1.sub(fract2, true);
                    System.out.println("");
                    Rational fract003 = fract1.div(fract2, true);
                    System.out.println("");

                    boolean t01 = fract1.lessThan(fract2);
                    if (t01) {
                        fract1.progressDisplayBoolean(fract1.original_chislitel,
                                fract1.original_znamenatel, fract2.original_chislitel,
                                fract2.original_znamenatel, 1);
                        System.out.println("");
                    }
                    boolean t02 = fract1.greaterThan(fract2);
                    if (t02) {
                        fract1.progressDisplayBoolean(fract1.original_chislitel,
                                fract1.original_znamenatel, fract2.original_chislitel,
                                fract2.original_znamenatel, 2);
                        System.out.println("");
                    }
                    boolean t03 = fract1.lessThanOrEqual(fract2);
                    if (t03) {
                        fract1.progressDisplayBoolean(fract1.original_chislitel,
                                fract1.original_znamenatel, fract2.original_chislitel,
                                fract2.original_znamenatel, 3);
                        System.out.println("");
                    }
                    boolean t04 = fract1.greaterThan(fract2);
                    if (t04) {
                        fract1.progressDisplayBoolean(fract1.original_chislitel,
                                fract1.original_znamenatel, fract2.original_chislitel,
                                fract2.original_znamenatel, 4);
                        System.out.println("");
                    }
                    System.out.println("");
                    break;

                case 3:

                    RationalAriphmetica ra = new RationalAriphmetica("task_3");
                    ra.rtlAriphm();
                    System.out.println(ra);
                    break;

                case 4:
                    RationalAriphmetica ra2 = new RationalAriphmetica("task_4");

                    ra2.to_Fract();
                    System.out.println(ra2);
                    break;

                case 5:
                    testStringUtils tSu = new testStringUtils();
                    tSu.testString();
            }

            System.out.println("Press any key to continue, or [X] to exit");
            String exitORnot = eon.readLine();
            if (exitORnot.equals("X") || exitORnot.equals("x")) {
                boo = false;
            } else boo = true;

        } while (boo);

    }
}




