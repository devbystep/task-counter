/**
 * Created by Icenord on 14.03.14.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


class counter {

    static int getDiffNumbers(int[] num) {
        int result = num[0];
        for (int i = 1; i < num.length; i++) {
            result -= num[i];
        }
        return result;
    }

    static float getProductNumbers(int[] num) {
        float result = 1;
        for (int i = 0; i < num.length; i++) {
            result *= num[i];
        }
        if (result == 0) {
            result = Math.abs(result);
        }
        return result;
    }

    static int getSumNumbers(int[] num) {
        int result = 0;

        for (int i = 0; i < num.length; i++) {
            result += num[i];
        }

        return result;
    }

    static int getMinNumber(int[] num) {
        int result = Integer.MAX_VALUE;

        for (int i = 0; i < num.length; i++) {
            if (num[i] < result) {
                result = num[i];
            }
        }

        return result;
    }

    static int getMaxNumber(int[] num) {
        int result = Integer.MIN_VALUE;

        for (int i : num) {
            if (i > result) {
                result = i;
            }
        }

        return result;
    }

    static float getMidNumber(int[] num) {
        float result = (float) counter.getSumNumbers(num) / num.length;
        return result;
    }

    static int readNumber(String hint) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(System.in));

        while (true) {
            try {
                System.out.print(hint);
                String line = bufferedReader.readLine();
                return Integer.parseInt(line);
            } catch (NumberFormatException e) {
                // ignore, but repeat the cycle
            }

            System.out.println("Invalid number was entered.");
        }
    }

    static int[] correctNumber(String[] argsString) {
        int arrayArgs[] = new int[argsString.length];
        int notAnumber = 0;

        for (int i = 0, a = 0; i < argsString.length; i++) {
            try {
                arrayArgs[a] = Integer.parseInt(argsString[i]);
                a++;
            } catch (NumberFormatException e) {
                notAnumber++;
            }
        }

        int newArrayArgs[] = new int[argsString.length - notAnumber];

        for (int b = 0; b < newArrayArgs.length; b++) {
            newArrayArgs[b] = arrayArgs[b];
        }

        System.out.println("There are command line numbers: ");

        for (int b : newArrayArgs) {
            System.out.print(b + " ");
        }
        System.out.println("");
        return newArrayArgs;
    }

    static void printResults() throws IOException {

        System.out.println("Command line not used! ");
        int argumentsnumber = counter
                .readNumber("\n" + "Enter a number of arguments:");
        int num[] = new int[argumentsnumber];
        System.out.println("");
        for (int a = 0; a < argumentsnumber; a++) {
            num[a] = counter.readNumber("Enter " + (a + 1) + " number: ");
        }
        System.out.println("\n" + "Result of product numbers: "
                + counter.getProductNumbers(num));
        System.out.println("Result of adding numbers: "
                + counter.getSumNumbers(num));
        System.out.println("Max number: " + counter.getMaxNumber(num));
        System.out.println("Min number: " + counter.getMinNumber(num));
        System.out.println("Mid number: " + counter.getMidNumber(num));
        System.out.println("Diff number: " + counter.getDiffNumbers(num));
    }

    static void printResults(int num[]) {

        System.out.println("Result of product numbers: "
                + counter.getProductNumbers(num));
        System.out.println("Result of adding numbers: "
                + counter.getSumNumbers(num));
        System.out.println("Max number: " + counter.getMaxNumber(num));
        System.out.println("Min number: " + counter.getMinNumber(num));
        System.out.println("Mid number: " + counter.getMidNumber(num));
        System.out.println("Diff number: " + counter.getDiffNumbers(num));
    }
}

