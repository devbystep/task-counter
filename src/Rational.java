/**
 * Created by Icenord on 14.03.14.
 */

import java.io.IOException;
import java.util.List;

public class Rational {
    int chislitel, znamenatel, original_chislitel, original_znamenatel;
    RationalProfiler profiler = new RationalProfiler();

    Rational(int chis, int znam) {
        chislitel = chis;
        znamenatel = znam;
    }

    Rational(int n, int chis, int znam, boolean b) throws IOException {
        original_chislitel = chis;
        original_znamenatel = znam;
        chislitel = chis;
        znamenatel = znam;
        normalizeFraction();
        if (b) {
            progressDisplay(chis, znam, n, 1);
        }
    }

    void progressDisplayBoolean(int chis, int znam, int chis_other,
                                int znam_other, int display) {

        switch (display) {

            case 1:
                System.out.print("Result fraction [<]: " + chis + "/" + znam
                        + " < " + chis_other + "/" + znam_other);
                break;
            case 2:
                System.out.print("Result fraction [>]: " + chis + "/" + znam
                        + " > " + chis_other + "/" + znam_other);
                break;
            case 3:
                System.out.print("Result fraction [=<]: " + chis + "/" + znam
                        + " =< " + chis_other + "/" + znam_other);
                break;
            case 4:
                System.out.print("Result fraction [>=]: " + chis + "/" + znam
                        + " >= " + chis_other + "/" + znam_other);

        }
    }

    private void progressDisplay(int chis, int znam, int n, int display) {

        switch (display) {

            case 1: // Вывод изначальной/упрощенной дроби (в теле конструктора)

                System.out.println("--------------------");

                if (chislitel == 0) {
                    System.out.println(n + " Fraction: " + chislitel);

                } else if (Math.abs(chislitel) == Math.abs(chis))
                    System.out.println(n + " Fraction: " + chislitel + "/"
                            + znamenatel);

                else
                    System.out
                            .println(n + " Fraction " + chis + "/" + znam
                                    + " simplificated to : " + chislitel + "/"
                                    + znamenatel);

                break;
            case 2: // Вывод финального результата
                if (chislitel / znamenatel == chislitel && chislitel != 0) {
                    System.out.print(chislitel + "/" + znamenatel + " = "
                            + chislitel);
                } else if (chislitel / znamenatel == 0 && chislitel != 0) {
                    System.out.print(chislitel + "/" + znamenatel);
                } else if (chislitel == 0)
                    System.out.print(chislitel);
                else
                    System.out.print(chislitel
                            + "/"
                            + znamenatel
                            + " = "
                            + chislitel
                            / znamenatel
                            + " "
                            + Math.abs(chislitel - (chislitel / znamenatel)
                            * znamenatel) + "/" + znamenatel);


                break;

            case 7: // Вывод финального результата для 4-ого задания
                if (chislitel / znamenatel == chislitel && chislitel != 0) {
                    System.out.print(" = "
                            + chislitel);
                } else if (chislitel / znamenatel == 0 && chislitel != 0) {
                    System.out.print(" = " + chislitel + "/" + znamenatel);
                } else if (chislitel == 0)
                    System.out.print(" = " + chislitel);
                else
                    System.out.print(" = "
                            + chislitel
                            / znamenatel
                            + " "
                            + Math.abs(chislitel - (chislitel / znamenatel)
                            * znamenatel) + "/" + znamenatel);

                break;


            case 3: // Вывод хода сложения: 1-ая дробь + 2-ая дробь
                if (chislitel == 0)
                    System.out.print("Result fraction [+]: " + chislitel + " + ");
                else
                    System.out.print("Result fraction [+]: " + chislitel + "/"
                            + znamenatel + " + ");
                if (chis == 0)
                    System.out.print(chis + " = ");
                else if (chis < 0)
                    System.out.print("(" + chis + "/" + znam + ")" + " = ");
                else
                    System.out.print(chis + "/" + znam + " = ");
                break;
            case 4: // Вывод хода умножения
                if (chislitel == 0)
                    System.out.print("Result fraction [*]: " + chislitel + " * ");
                else
                    System.out.print("Result fraction [*]: " + chislitel + "/"
                            + znamenatel + " * ");
                if (chis == 0)
                    System.out.print(chis + " = ");
                else if (chis < 0)
                    System.out.print("(" + chis + "/" + znam + ")" + " = ");
                else
                    System.out.print(chis + "/" + znam + " = ");
                break;
            case 5: // Вывод хода вычитания: 1-ая дробь - 2-ая дробь
                if (chislitel == 0)
                    System.out.print("Result fraction [-]: " + chislitel + " - ");
                else
                    System.out.print("Result fraction [-]: " + chislitel + "/"
                            + znamenatel + " - ");
                if (chis == 0)
                    System.out.print(chis + " = ");
                else if (chis < 0)
                    System.out.print("(" + chis + "/" + znam + ")" + " = ");
                else
                    System.out.print(chis + "/" + znam + " = ");
                break;
            case 6: // Вывод хода деления
                if (chislitel == 0)
                    System.out.print("Result fraction [/]: " + chislitel + " * ");
                else
                    System.out.print("Result fraction [/]: " + chislitel + "/"
                            + znamenatel + " / ");
                if (chis == 0)
                    System.out.print(chis + " = ERROR :: Division by ZERO!!!!");
                else if (chis < 0)
                    System.out.print("(" + chis + "/" + znam + ")" + " = ");

                else
                    System.out.print(chis + "/" + znam + " = ");
                break;

        }
    }

    private void normalizeFraction() {
        if (znamenatel < 0) {
            znamenatel *= -1;
            chislitel *= -1;
        }

        int n = naibolsh_Obsch_Delitel(chislitel, znamenatel);
        znamenatel = znamenatel / n;
        chislitel = chislitel / n;
    }

    static int getChislitel() throws IOException {
        int chislitel = counter.readNumber("\n" + "Enter chislitel: ");
        return chislitel;
    }

    static int getZnamenatel() throws IOException {
        int znamenatel = counter.readNumber("Enter znamenatel: ");
        if (znamenatel == 0) {
            System.out.println("Znamenatel = NULL!!!!!");
            znamenatel = Rational.getZnamenatel();
        }
        return znamenatel;
    }

    int naibolsh_Obsch_Delitel(int a, int b) {
        if (a % b == 0)
            return Math.abs(b);
        return naibolsh_Obsch_Delitel(b, a % b);
    }

    int naimensh_Obsch_Kratnoe(int a, int b) {
        return (a / naibolsh_Obsch_Delitel(a, b));
    }

    public Rational add(Rational fraction_other, boolean b, boolean resultOnly) throws IOException {
        int temp_chislitel = chislitel;
        int temp_znamenatel = znamenatel;
        if (b) {
            progressDisplay(fraction_other.chislitel,
                    fraction_other.znamenatel, 0, 3);
        }
        chislitel = chislitel * fraction_other.znamenatel
                + fraction_other.chislitel * znamenatel;
        znamenatel = znamenatel * fraction_other.znamenatel;
        normalizeFraction();
        if (b) {
            progressDisplay(0, 0, 0, 2);
        }
        if (resultOnly) {
            progressDisplay(0, 0, 0, 7);
        }
        Rational temp_fract = new Rational(chislitel, znamenatel);
        chislitel = temp_chislitel;
        znamenatel = temp_znamenatel;
        return temp_fract;
    }

    public Rational mult(Rational fraction_other, boolean b) throws IOException {
        int temp_chislitel = chislitel;
        int temp_znamenatel = znamenatel;
        if (b) {
            progressDisplay(fraction_other.chislitel,
                    fraction_other.znamenatel, 0, 4);
        }
        chislitel = chislitel * fraction_other.chislitel;
        znamenatel = znamenatel * fraction_other.znamenatel;
        normalizeFraction();
        if (b) {
            progressDisplay(0, 0, 0, 2);
        }
        Rational temp_fract = new Rational(chislitel, znamenatel);
        chislitel = temp_chislitel;
        znamenatel = temp_znamenatel;
        return temp_fract;
    }

    public Rational sub(Rational fraction_other, boolean b) throws IOException {
        int temp_chislitel = chislitel;
        int temp_znamenatel = znamenatel;
        if (b) {
            progressDisplay(fraction_other.chislitel,
                    fraction_other.znamenatel, 0, 5);
        }
        chislitel = chislitel * fraction_other.znamenatel
                - fraction_other.chislitel * znamenatel;
        znamenatel = znamenatel * fraction_other.znamenatel;
        normalizeFraction();
        if (b) {
            progressDisplay(0, 0, 0, 2);
        }
        Rational temp_fract = new Rational(chislitel, znamenatel);
        chislitel = temp_chislitel;
        znamenatel = temp_znamenatel;
        return temp_fract;
    }

    public Rational subForBoolean(Rational fraction_other) throws IOException {
        int temp_chislitel = chislitel;
        int temp_znamenatel = znamenatel;

        chislitel = chislitel * fraction_other.znamenatel
                - fraction_other.chislitel * znamenatel;
        znamenatel = znamenatel * fraction_other.znamenatel;
        // normalizeFraction();
        Rational temp_fract = new Rational(chislitel, znamenatel);
        chislitel = temp_chislitel;
        znamenatel = temp_znamenatel;
        return temp_fract;
    }

    public Rational div(Rational fraction_other, boolean b) throws IOException {
        int temp_chislitel = chislitel;
        int temp_znamenatel = znamenatel;
        if (b) {
            progressDisplay(fraction_other.chislitel,
                    fraction_other.znamenatel, 0, 6);
        }
        if (fraction_other.chislitel == 0) {

            while (fraction_other.chislitel == 0) {
                fraction_other.chislitel = getChislitel();
                fraction_other.znamenatel = getZnamenatel();
            }
            progressDisplay(fraction_other.chislitel,
                    fraction_other.znamenatel, 0, 6);
        }
        chislitel = chislitel * fraction_other.znamenatel;
        znamenatel = znamenatel * fraction_other.chislitel;

        normalizeFraction();
        if (b) {
            progressDisplay(0, 0, 0, 2);
        }
        Rational temp_fract = new Rational(chislitel, znamenatel);
        chislitel = temp_chislitel;
        znamenatel = temp_znamenatel;

        return temp_fract;
    }

    boolean lessThan(Rational fraction_other) throws IOException {
        boolean t = true;
        final Rational fract1temp = new Rational(chislitel, znamenatel);
        final Rational fract2temp = new Rational(fraction_other.chislitel,
                fraction_other.znamenatel);
        Rational lessThanFract = fract1temp.subForBoolean(fract2temp);
        if (lessThanFract.chislitel < 0) {
            profiler.addProfilerString("Result fraction [<]: "
                    + fract1temp.chislitel + "/" + fract1temp.znamenatel + "<"
                    + fract2temp.chislitel + "/" + fract2temp.znamenatel);

        } else
            t = false;

        return t;
    }

    boolean greaterThan(Rational fraction_other) throws IOException {
        boolean t = true;
        Rational fract1temp = new Rational(chislitel, znamenatel);
        Rational fract2temp = new Rational(fraction_other.chislitel,
                fraction_other.znamenatel);
        Rational greaterThanFract = fract1temp.subForBoolean(fract2temp);
        if (greaterThanFract.chislitel > 0) {
            profiler.addProfilerString("Result fraction [>]: "
                    + fract1temp.chislitel + "/" + fract1temp.znamenatel
                    + " > " + fract2temp.chislitel + "/"
                    + fract2temp.znamenatel);

        } else
            t = false;

        return t;
    }

    boolean lessThanOrEqual(Rational fraction_other) throws IOException {
        boolean t = true;
        Rational fract1temp = new Rational(chislitel, znamenatel);
        Rational fract2temp = new Rational(fraction_other.chislitel,
                fraction_other.znamenatel);
        Rational lessThanOrEqualFract = fract1temp.subForBoolean(fract2temp);
        if (lessThanOrEqualFract.chislitel <= 0) {
            profiler.addProfilerString("Result fraction [=<]: "
                    + fract1temp.chislitel + "/" + fract1temp.znamenatel
                    + " =< " + fract2temp.chislitel + "/"
                    + fract2temp.znamenatel);

        } else
            t = false;

        return t;
    }

    boolean greaterThanOrEqual(Rational fraction_other) throws IOException {
        boolean t = true;
        Rational fract1temp = new Rational(chislitel, znamenatel);
        Rational fract2temp = new Rational(fraction_other.chislitel,
                fraction_other.znamenatel);
        Rational lessThanOrEqualFract = fract1temp.subForBoolean(fract2temp);
        if (lessThanOrEqualFract.chislitel >= 0) {
            profiler.addProfilerString("Result fraction [=>]: "
                    + fract1temp.chislitel + "/" + fract1temp.znamenatel
                    + " =< " + fract2temp.chislitel + "/"
                    + fract2temp.znamenatel);

        } else
            t = false;

        return t;
    }

    void simplyPrint(String s) {
        System.out.print(s);
    }

    public List<String> getProfilerStringList() {
        return profiler.getProfilerStringList();
    }

}