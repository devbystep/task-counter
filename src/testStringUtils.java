/**
 * Created by Icenord on 14.03.14.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class testStringUtils {
    List<String> stringTOarray = new ArrayList<String>();
    List<String> stringTEMP = new ArrayList<String>();
    char ch[] = new char[100];
    String string123 = "qwETtyw12345698ws";
    String stringForCampare = "compaRestring777";
    String st = "ty7";
    String string123Space = "   qwE Ttyw 12345 698ws";

    int returnN(int a, int b) {
        int c;
        if ((a - b) == 1) c = 1;
        else c = 2;
        return c;
    }

    @SuppressWarnings("unchecked")
    public void testString() {
        // char java.lang.String.charAt(int index)
        // Вывод конкретного символа строки по индексу

        System.out.println("Производятся действия над строкой: " + string123);
        System.out.println("Вспомогательная строка: compaRestring777\n\n");
        for (int i = 0; i < string123.length(); i++) {
            if (i < 10)
                System.out.print("'" + string123.charAt(i) + "'" + ":[ " + string123.indexOf(string123.charAt(i), i) + "] ");
            else
                System.out.print("'" + string123.charAt(i) + "'" + ":[" + string123.indexOf(string123.charAt(i), i) + "] ");
            if (i % 4 == 0) System.out.println("");
        }

        System.out.println("\nСАМ 3-ий символ из строки, начиная с [0].\ncharAt(3)): " + string123 + ": "
                + string123.charAt(3));

        System.out.println("\nКОД третьего символа.\ncodePointAt(3)" + string123 + ": "
                + string123.codePointAt(3));

        System.out.println("\nКод символа перед символом.\ncodePointBefore(3)" + string123.charAt(3) + ": "
                + string123.codePointBefore(3));

        System.out.println("\ncodePointCount(3, 6): " + string123.codePointCount(6, 6));
        System.out.println("\nСравнение двух строк лексиграфически.\ncompareTo(comparestring777): " + string123.compareTo(string123));
        System.out.println("\nСравнение двух строк лексиграфически.\nБез учета регистра " +
                "compareTo(comparestring777): " + string123.compareToIgnoreCase(string123));
        System.out.println("\nconcat(stringForCampare).\nДобавляет к строке строку: " + string123.concat(stringForCampare));
        System.out.println("\ncontains(rty).\nИщет в строке последовательность rty: " + string123.contains("rty"));
        System.out.println("\nhashCode().\nВозвращает хэшкод для данной строки: " + string123.hashCode());
        System.out.println("\nindexOf(s).\nВозвращает индекс внутри данной строки первого\nвхождения указанного символа: " + string123.indexOf("s"));
        System.out.println("\nindexOf(stringForCampare) ty7.\nВозвращает индекс внутри данной\nстроки первого вхождения " +
                "указанной подстроки.: " + string123.indexOf(st));
        System.out.println("\nstring123.indexOf(7, 8).\nВозвращает индекс внутри данной строки первого вхождения\nуказанного символа (7), начиная поиск " +
                "с указанной позиции (8): " + string123.indexOf("7", 8));
        System.out.println("\nstring123.intern().\nВозвращает каноническое представление для данного\nстрокового объекта: " + string123.intern());
        System.out.println("\nlastIndexOf(s).\nВозвращает индекс внутри данной строки последнего\nвхождения указанного символа: " + string123.lastIndexOf("w"));
        System.out.println("\nstring123.replace(w, V).\nВозвращает новую строку, в которой заменены все\nвхождения символа oldChar в данной строке на\nсимвол newChar: " + string123.replace("w", "V"));
        System.out.println("\ntoLowerCase().\nКонвертирует все символы данной строки в нижний регистр,\nиспользуя правила локали по умолчанию: " + string123.toLowerCase());
        System.out.println("\nstring123Space.trim().\nДо: " + string123Space + " После: " + string123Space.trim());
        System.out.println("\nstring123.toCharArray(): " + Arrays.toString(string123.toCharArray()));
        System.out.println("\nstring123.charAt(0): " + string123.charAt(0));


        for (int r = 0; r < string123.length(); r++) {
        }


        stringTOarray.add(string123);


        System.out.println("\nstringTOarray.add(string123): " + stringTOarray);
        //string123.


        for (int i = 0; i < string123.length(); i++) {


            stringTEMP.add(i, new String(string123.toCharArray(), i, returnN(string123.length(), i)));
        }

        System.out.print(stringTEMP);
    }


}

