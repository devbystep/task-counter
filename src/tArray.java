/**
 * Created by Icenord on 14.03.14.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class tArray {
    public void teachArray() {
        List<Integer> dPoints = new ArrayList<Integer>();

        dPoints.add(94);
        dPoints.add(53);
        dPoints.add(70);
        dPoints.add(44);
        dPoints.add(64);


        Integer[] points = new Integer[5];
        points[0] = 94;
        points[1] = 53;
        points[2] = 70;
        points[3] = 44;
        points[4] = 64;

        String[] arString = new String[6];
        arString[0] = "One";
        arString[1] = "Two";
        arString[2] = "Three";
        arString[3] = "Four";
        arString[4] = "Five";
        arString[5] = "Six";

        // Sort the points array, the default order is in ascending order.
        // [44, 53, 64, 70, 94]

        Arrays.sort(points);
        System.out.println(Arrays.toString(points));


        // Sort the points array in descending order.
        // [94, 70, 64, 53, 44]

        Arrays.sort(points, Collections.reverseOrder());
        System.out.println(Arrays.toString(points));

        Arrays.sort(arString, Collections.reverseOrder());
        // System.out.println(arString[0]);
        System.out.println(Arrays.toString(arString));

	/*
     *
	 * да. твой массив статический и ты можеш только удалить содержание, заменить его нулем и т.п.
	 * но не можеш просто выкинуть его . ты ему отвел уже память. понял. можешь мне здесь маленький динамический добавить?
		сейчас добаввлю
	 */

        //вставляю код в конец проги что бы не путать выводимую информацию
        // это какая задача ?
        System.out.println("before");
        for (int i = 0; i < dPoints.size(); i++) {
            System.out.println(dPoints.get(i));
        }

        dPoints.remove((Object) 70); // удаляет объект из памяти равный 70 в данном чслучае 70 это не индекс объекта а сам объект
        dPoints.remove(70);// удаляет объект под номером 70 в списке генерирует ошибку потому что список не содержит 70 объектов

        System.out.println("after");

        for (int i = 0; i < dPoints.size(); i++) {
            System.out.println(dPoints.get(i));
        }


        System.out.println(Arrays.toString(dPoints.toArray()));

    }
}


