/**
 * Created by Icenord on 14.03.14.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RationalAriphmetica {

    String forTOstring, testTOstring;
    String toFract;
    String splitStrThis[];
    boolean minusWith;
    Rational fractSplit1;

    RationalAriphmetica(String fts) {
        forTOstring = fts;
    }

    public void rtlAriphm() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        List<Rational> massFract = new ArrayList<Rational>();
        List<String> massAction = new ArrayList<String>();
        String stringForStop;
        String action;
        int indexTemp = 0;
        boolean whileTf = true;
        int indexFraction = 0;
        int indexAction = 0;
        boolean tr = true;
        boolean rightKey;

        massFract.add(new Rational(++indexFraction, Rational.getChislitel(),
                Rational.getZnamenatel(), true));

        while (tr) {

            // ---------------------------------------------------------------------------
            for (int valid = 0; valid != 1; ) {
                System.out
                        .println("\nEnter an ariphmetical operation [/], [*], [+], [-]: ");
                action = br.readLine();
                if (action.equals("/") || action.equals("*")
                        || action.equals("+") || action.equals("-")) {
                    massAction.add(action);
                    indexAction++;
                    valid = 1;
                    System.out.print("Entered operation: [" + action + "]\n");
                } else {
                    System.out
                            .println("\nEnter a valid ariphmetical operation!!! ");
                }
            }
            // ---------------------------------------------------------------------------

            massFract.add(new Rational(indexFraction++,
                    Rational.getChislitel(), Rational.getZnamenatel(), true));

            System.out
                    .print("\nPress [C/c] to continue operation\nor [S/s] to see the result of action: ");

            for (int i = 0, a = 0; i < indexFraction; i++) {


                System.out.print("[" + massFract.get(i).original_chislitel
                        + "/" + massFract.get(i).original_znamenatel + "]");
                if (a < indexAction)
                    System.out.print(" " + massAction.get(a++) + " ");
            }

            System.out.println("");
            rightKey = true;
            while (rightKey) {
                stringForStop = br.readLine();
                if (stringForStop.equals("S") || stringForStop.equals("s")) {
                    tr = false;
                    rightKey = false;
                } else if (stringForStop.equals("C")
                        || stringForStop.equals("c")) {
                    rightKey = false;
                } else {
                    System.out.print("Invalid key pressed!! ");

                }
            }

        }// while (tr)

        while (whileTf || indexTemp != massAction.size()) {
            // System.out.print(" " + massAction.get(i) + " ");
            whileTf = false;
            if (massAction.get(indexTemp).equalsIgnoreCase("*")) {

                massFract.set(
                        indexTemp,
                        massFract.get(indexTemp).mult(
                                massFract.get(indexTemp + 1), true));
                System.out.println("");
                massFract.remove(indexTemp + 1);
                massAction.remove(indexTemp);
                whileTf = true;
                if (indexTemp == massAction.size()) {
                    whileTf = false;
                }

            } else if

                    (massAction.get(indexTemp).equalsIgnoreCase("/")) {

                massFract.set(
                        indexTemp,
                        massFract.get(indexTemp).div(
                                massFract.get(indexTemp + 1), true));
                System.out.println("");
                massFract.remove(indexTemp + 1);
                massAction.remove(indexTemp);
                whileTf = true;
                if (indexTemp == massAction.size()) {
                    whileTf = false;
                }

            } else {

                indexTemp++;

            }
        } // while (whileTf * or /)

        if (massAction.size() != 0) {
            whileTf = true;
            indexTemp = 0;
            while (whileTf) {
                // System.out.print(" " + massAction.get(i) + " ");
                whileTf = false;
                if (massAction.get(indexTemp).equalsIgnoreCase("+")) {

                    massFract.set(
                            indexTemp,
                            massFract.get(indexTemp).add(
                                    massFract.get(indexTemp + 1), true, false));
                    System.out.println("");
                    massFract.remove(indexTemp + 1);
                    massAction.remove(indexTemp);
                    whileTf = true;
                    if (indexTemp == massAction.size()) {
                        whileTf = false;
                    }

                } else if

                        (massAction.get(indexTemp).equalsIgnoreCase("-")) {

                    massFract.set(
                            indexTemp,
                            massFract.get(indexTemp).sub(
                                    massFract.get(indexTemp + 1), true));
                    System.out.println("");
                    massFract.remove(indexTemp + 1);
                    massAction.remove(indexTemp);
                    whileTf = true;
                    if (indexTemp == massAction.size()) {
                        whileTf = false;
                    }

                } else {
                    indexTemp++;
                }
            }

        } // while (whileTf +/-)

        // System.out.println(massFract.get(4).original_chislitel);
        // System.out.println(Arrays.toString(massAction.toArray()));

    } // public void rtlAriphm() throws IOException

    public void to_Fract() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        boolean d = true;
        String continueEntering;

        // while (d)
        try_else:
        while (d) {
            System.out.println("\n" + "Enter a correct rational: ");

            boolean t = true;

            // while (t)
            while (t) {
                toFract = br.readLine();


                try {
                    if (toFract.toCharArray()[0] == '-') {
                        minusWith = true;

                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    continue try_else;
                }
                // Проверка на повторение более одной запятой
                for (int i = 0, countComma = 0; i < toFract.toCharArray().length; i++) {
                    if (toFract.toCharArray()[i] == ',') {
                        countComma++;
                    }
                    if (countComma > 1)
                        continue try_else;
                }

                // Проверка на неправильный ввод данных типа: ,5 или 5, или
                // НЕцифры
                for (int i = 0; i < toFract.toCharArray().length; i++) {
                    if (toFract.toCharArray()[i] == ',') {
                        if (i == 0 || i == toFract.toCharArray().length - 1)
                            continue try_else;
                    } else if (!toFract.contains(",")) {
                        try {
                            Integer.parseInt(toFract);

                        } catch (NumberFormatException e) {
                            continue try_else;
                        }

                    }
                }

                if (toFract.contains(",")) {
                    t = false;
                } else {
                    System.out.println("Comma exist!!");
                    Random r = new Random();
                    System.out
                            .println("Please, write again with a comma (For example "
                                    + toFract
                                    + ","
                                    + Integer.toString(Math.abs(r.nextInt()))
                                    .subSequence(0, 3) + "):");
                }
            }
            // while (t)

            String splitStr[] = toFract.split(",");
            splitStrThis = splitStr;

            try {
                Integer.parseInt(splitStr[0]);

            } catch (NumberFormatException e) {
                System.out.println("Wrong ratianal! Please write correct!! ");
                System.out.println("Look for mistake in " + "[" + splitStr[0]
                        + "] at first");

                continue try_else;
            }
            try {
                Integer.parseInt(splitStr[1]);
            } catch (NumberFormatException e) {
                System.out.println("Wrong rational! Please write correct!! ");
                System.out.println("Look for mistake in " + "[" + splitStr[1]
                        + "]");
                continue try_else;
            }
            d = false;
        } // while (d)


        Rational fractSplit1 = new Rational(Integer.parseInt(splitStrThis[0]),
                1);
        fractSplit1.simplyPrint(toFract);

        int splitStrThisInt = Integer.parseInt(splitStrThis[1]);

        if (splitStrThis.length < 2) {
            Rational fractSplit2 = new Rational(0, 1);

            Rational finalFract = fractSplit1.add(fractSplit2, false, true);

        } else {
            Double b = Math.pow((double) 10, (double) splitStrThis[1].length());
            Rational fractSplit2 = new Rational(
                    Integer.parseInt(splitStrThis[1]), b.shortValue());

            if (minusWith) {
                Rational multFract = new Rational(1, -1);
                Rational finalFract = fractSplit1.add(
                        fractSplit2.div(multFract, false), false, true);

            } else {
                Rational finalFract = fractSplit1.add(fractSplit2, false, true);
            }
        }

        System.out
                .print("\nPress [C/c] to continue operation\nor [X/x] to exit: ");

        boolean rightKey = true;
        while (rightKey) {
            continueEntering = br.readLine();
            if (continueEntering.equals("X") || continueEntering.equals("x")) {

                rightKey = false;
            } else if (continueEntering.equals("C")
                    || continueEntering.equals("c")) {

                rightKey = false;
                to_Fract();

            } else {
                System.out.print("Invalid key pressed!! ");

            }
        }

    }

    public String toString() {
        if (forTOstring.equals("task_3")) {
            testTOstring = "task_3";
        } else if (forTOstring.equals("task_4")) {
            testTOstring = "task_4";
        }

        return testTOstring + " in progress";
    }
}
